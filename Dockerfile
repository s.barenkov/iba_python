FROM python:3.8
WORKDIR /home/andrei/PycharmProjects/iba_python
RUN pip3 install flask
RUN pip3 install pandas
RUN pip3 install ibm-db
COPY . .
EXPOSE 5000
CMD ["service.py"]
ENTRYPOINT ["python3"]