N = int(input('Enter number of alarm clocks: '))
X = int(input('Enter number of minutes after alarming: '))
K = int(input('Enter number of alarm clocks for wake up: '))
t = []
t_rediv = []
t_div = {}
for i in range(N):
    t.append(int(input('Enter time moment: ')))
print(t)
t = sorted(t)
for i in t:
    t_div = [i % N for i in t]
for N in t_div:
    t_rediv.append(N)
t = t_rediv
print(t)
T = lambda time, t_div: sum([(time - t) // X for t in t_div])

def LowerBound(A, key):
    left = -1
    right = len(A)
    while right > left + 1:
        middle = (left + right) // 2
        if A[middle] >= key:
            right = middle
        else:
            left = middle
    return right

print(LowerBound(t, K))
