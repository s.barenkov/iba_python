import random

N = int(input('Number of cards: '))
K = int(input('Score for winning: '))
p_score = 0
v_score = 0

for i in range(0, N):
    print('Step: {}'.format(i+1))
    card = random.randint(0, 100)
    print('Card point is: {}'.format(card))
    if card % 5 == 0 and card % 3 == 0:
        print('no points in this round')
        continue
    elif card % 5 == 0:
        v_score += 1
        print('Vasya score: {}'.format(v_score))
        if v_score == K:
            print('Vasya win!')
            break
    elif card % 3 == 0:
        p_score += 1
        print('Petya score: {}'.format(p_score))
        if p_score == K:
            print('Petya win!')
            break
    elif i == N-1:
        if p_score > v_score:
            print('Petya win with the score: {}!'.format(p_score))
        else:
            print('Vasya win with the score: {}!'.format(v_score))
    else:
        print('no points in this round')
        continue
