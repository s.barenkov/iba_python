from datetime import datetime, timedelta, time

import requests
import ibm_db_dbi as db
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.http.sensors.http import HttpSensor
from bs4 import BeautifulSoup

time_start = time(9)
default_args = {
            "owner": "User11",
            "start_date": time_start,
            "depends_on_past": False,
            "email_on_failure": False,
            "email_on_retry": False,
            "email": "youremail@host.com",
            "retries": 1,
            "retry_delay": timedelta(minutes=5)
        }

URL = 'http://seasonvar.ru'
HEADERS = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                         'Chrome/95.0.4638.69 Safari/537.36',
           'Accept': '*/*'}


def get_html(url, params=None):
    request = requests.get(url, headers=HEADERS, params=params)
    return request


def get_pages(link):
    href = link.get('href')
    page_link = URL + href
    return page_link


def get_link_content(link):
    html_links = get_html(get_pages(link))
    soup = BeautifulSoup(html_links.text, 'html.parser')
    div = soup.find('div', attrs={'class': 'pgs-sinfo'})
    return div


def get_genre(block):
    genre = block.find('span', attrs={'itemprop': 'genre'}).get_text(strip=True)
    return genre


def get_release_date(block):
    div = block.find_all('div', attrs={'class': 'pgs-sinfo_list'})[2]
    release_date = div.find('span').get_text(strip=True)
    return release_date


def get_directors(block):
    div = block.find('div', attrs={'itemprop': 'directors'})
    if div is None:
        str = 'Unknown directors'
        return str
    else:
        div = block.find('div', attrs={'itemprop': 'directors'})
        str = ''
        directors = div.find_all('span', attrs={'itemprop': 'name'})
        if len(directors) > 1:
            for d in directors:
                if d == directors[-1]:
                    str += d.get_text(strip=True)
                else:
                    content = '{}, '
                    str += content.format(d.get_text(strip=True))
        else:
            for d in directors:
                str += d.get_text(strip=True)
        return str


def get_imdb_rating(block):
    div = block.find_all('div', attrs={'class': 'rating'})
    if len(div) == 0:
        rating = 'Unknown IMDB rating'
        return rating
    else:
        rating = div[0].find('span').get_text(strip=True)
        return rating


def get_kinopoisk_rating(block):
    div = block.find_all('div', attrs={'class': 'rating'})
    if len(div) <= 1:
        rating = 'Unknown KinoPoisk rating'
        return rating
    else:
        rating = div[1].find('span').get_text(strip=True)
        return rating


def get_extended_info(link):
    content = get_link_content(link)
    genre = get_genre(content)
    release_date = get_release_date(content)
    directors = get_directors(content)
    imdb_rating = get_imdb_rating(content)
    kinopoisk_rating = get_kinopoisk_rating(content)
    return genre, release_date, directors, imdb_rating, kinopoisk_rating


def get_serials(serials, link, item):
    genre, release_date, directors, imdb_rating, kinopoisk_rating = get_extended_info(link)
    serials.append({
        'date': item.find('div', attrs={'class': 'news-head'}).get_text(strip=True),
        'title': link.find('div', attrs={'class': 'news_n'}).get_text(strip=True),
        'series': link.find('span', attrs={'class': 'news_s'}).get_text(strip=True),
        'genre': genre,
        'release date': release_date,
        'director(s)': directors,
        'IMDB rating': imdb_rating,
        'KinoPoisk rating': kinopoisk_rating
    })


def delta_func(html):
    soup = BeautifulSoup(html, 'html.parser')
    items = soup.find_all('div', attrs={'class': 'news'})
    serials = []
    for item in range(len(items)):
        if item == 1:
            links = items[item].find_all('a')
            for l in links:
                get_serials(serials, l, items[item])
    return serials


def download_serials(**kwargs):
    last_run = kwargs["prev_execution_date"]
    last_run = datetime.fromtimestamp(last_run.int_timestamp)
    curr_run = kwargs["execution_date"]
    curr_run = datetime.fromtimestamp(curr_run.int_timestamp)

    if not last_run:
        last_run = datetime(1970, 1, 1)

    conn = db.connect('DATABASE=IBA_EDU;'
                      'HOSTNAME=3d-edu-db.icdc.io;'
                      'PORT=8163;'
                      'PROTOCOL=TCPIP;'
                      'UID=stud11;'
                      'PWD=12345;', '', '')
    cur = conn.cursor()

    html = get_html(URL)
    content = []
    if html.status_code == 200:
        content.extend(delta_func(html.text))
    else:
        print('error')
        return 1

    sql = '''INSERT INTO STUD11.PARSER_TEST(Post_Date, Title, Series, Genre, Release_Date, Directors,
    IMDB_Rating, KinoPoisk_Rating) VALUES (?, ?, ?, ?, ?, ?, ?, ?)'''

    pmValues = []
    for i in content:
        pmValues.append(list(i.values()))
    print(pmValues)

    try:
        for i in range(len(pmValues)):
            resultSet = cur.execute(sql, pmValues[i])
    except Exception as e:
        print(e)
        print('error!')

    cur.execute('COMMIT')

    cur.execute('ROLLBACK')

    cur.close()
    conn.close()


with DAG(dag_id='serials_pipeline', schedule_interval='@daily', default_args=default_args, catchup=False) as dag:
    is_web_available = HttpSensor(
        task_id='is_web_available',
        method='GET',
        http_conn_id='u11_season',
        endpoint='',
        response_check=lambda response: True if response.status_code == 200 else False,
        poke_interval=5,
        timeout=20
    )
    downloading_serials = PythonOperator(
        task_id='download_serials',
        provide_context=True,
        python_callable=download_serials
    )

    is_web_available >> downloading_serials
