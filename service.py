from datetime import date

from flask import Flask, render_template
import pandas as pd
import ibm_db_dbi as db

app = Flask(__name__)


@app.route('/', methods=['GET'])
def service():
    conn = db.connect('DATABASE=IBA_EDU;'
                      'HOSTNAME=3d-edu-db.icdc.io;'
                      'PORT=8163;'
                      'PROTOCOL=TCPIP;'
                      'UID=stud11;'
                      'PWD=12345;', '', '')
    df = pd.read_sql(sql="SELECT * FROM STUD11.PARSER_TEST",
                     con=conn)
    conn.close()
    return df.to_html(header="true", table_id="table")


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
